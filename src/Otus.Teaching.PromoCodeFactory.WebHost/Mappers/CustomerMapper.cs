using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {
        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if (customer == null)
            {
                customer = new Customer();
            }
            
            customer.Email = model.Email;
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            
            if (preferences.Any())
            {
                customer.Preferences = preferences.Select(x => new CustomerPreference
                {
                    Customer = customer,
                    Preference = x
                }).ToList();
            }
            
            return customer;
        }
    }
}